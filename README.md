# Software Studio 2018 Spring Midterm Project

## Topic
* [Complex Forum]
* Key functions
    1. [user page]
    2. [post page]
    3. [post list page]
    4. [leave comments under any post]
* Other functions 
    1. [home page]
    2. [random background image for home page]
    3. [thumb up for posts]
    4. [most thumb-up post shows on home page]
	5. [show commit time of posts and comments]
	6. [posts listed from new to old]
	7. [show how many thumb ups a post received]
	8. [comment or thumb up a new post without reload page]
	9. [log the number of posts and comments a user commits]
	10. [log the number of thumb ups a user received]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
	1. Introduction: Complex forum有著比Simple forum更多樣的功能，卻不失原本的簡潔。
	2. Home page: 每次載入都會隨機選擇背景圖片(10張)。簡單地以圖文介紹Complex forum的功能，登入後的使用者在最下方會看到讚數最多的貼文。若有新貼文，會出現notification。
	3. Posts list: 必須登入才能瀏覽貼文。由上而下、由新至舊顯示貼文，貼文包含標題、內容、作者、上傳時間、收到的讚數、評論。即時更新貼文、評論、收到的讚數，並且可以和新貼文互動，不需要重新載入頁面。貼文藉由css animation會逐漸浮出。
	4. New post: 必須登入才能新增貼文。讓使用者輸入貼文標題、內容，發表之後回到主頁。
	5. Profile: 必須登入才能得到使用者資料。記錄使用者信箱、貼文數、評論數、收到的讚數。
	6. Signin page: 提供兩種登入方式，信箱註冊、google登入。登入後上方的navigation bar會有登出的選項，並且會顯示使用者信箱。
## Security Report 
	將firebase database的read、write規則設定為auth!=null，僅讓註冊並登入後的使用者能讀寫資料。