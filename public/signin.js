function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function () {
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(function () {
                window.location = "index.html";
            })
            .catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                create_alert("error", errorMessage);
                txtEmail.value = "";
                txtPassword.value = "";
            });
    });

    btnGoogle.addEventListener('click', function () {
        
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider)
            .then(function () {
                var userRef = firebase.database().ref('users');
                userRef.once('value').then(function(snapshot){
                    var loginUser = firebase.auth().currentUser;
                    if(snapshot.child(loginUser.uid).exists() == false){
                        firebase.database().ref('users/'+loginUser.uid).set({
                            email: loginUser.email,
                            nickname: "",
                            posts : 0,
                            comments: 0,
                            thumb_ups: 0
                        })
                    }
                })
                window.location = "index.html";
            })
            .catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                create_alert("error", errorMessage);
                txtEmail.value = "";
                txtPassword.value = "";
            });
    });

    btnSignUp.addEventListener('click', function () {
        console.log(87);
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(function () {
                create_alert("success", "You could sign in  right now!");
                var loginUser = firebase.auth().currentUser;
                firebase.database().ref('users/' + loginUser.uid).set({
                    email: loginUser.email,
                    nickname: "",
                    posts : 0,
                    comments: 0,
                    thumb_ups: 0
                })
                txtEmail.value = "";
                txtPassword.value = "";
            })
            .catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                create_alert("error", errorMessage);
                txtEmail.value = "";
                txtPassword.value = "";
            });
    });

}

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "Success! " + message;
        alert(str_html);
        //alertarea.innerHTML = str_html;
    }
    else if (type == "error") {
        str_html = "Error! " + message;
        alert(str_html);
        //alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
}