function init(){
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var Sign_in = document.getElementById('signin');
        var navBar = document.getElementById('nav');
        if (user) {
            user_email = user.email;
            Sign_in.innerHTML = "<a href='' id='signout'>Signout</a>";
            var Sign_out = document.getElementById('signout');
            var userInfo = document.getElementById('userInfo');
            userInfo.innerHTML += "<li><a href=''>"+user_email+"</a></li>";
            Sign_out.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });

            var postsRef = firebase.database().ref("post_list");
            var total_post = [];
            var first_count = 0;
            var second_count = 0;
            postsRef.once('value').then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var childData = childSnapshot.val();
                    total_post[total_post.length] = "<div class='my-5 p-3 article'><h2>"+childData.title+"</h2><h3>"+childData.content+"</h3><div><p class='thumb_ups' id='thumb_ups"+first_count.toString()+"'>"+childData.thumb_ups+"</p><a class='btn' href='#' onClick='return false;' id='thumb_btn"+first_count.toString()+"'><i class='far fa-thumbs-up'></i></a><h6>"+childData.email+"</h6></div><h6>"+childData.time+"</h6><div id='comments"+first_count.toString()+"'></div><textarea id='text"+first_count.toString()+"' class='form-control' rows=2 placeholder='comment here'></textarea><button id='comment_btn"+first_count.toString()+"' type='button' class='btn btn-primary mt-3 float-right'>Submit</button><br><br></div>\n";
                    first_count += 1;
                });
                document.getElementById('post_list').innerHTML = total_post.reverse().join('');

                //add listener
                postsRef.on('child_added', function (data) {
                    second_count += 1;
                    if (second_count > first_count) {
                        var childData = data.val();
                        total_post[total_post.length] = "<div class='my-5 p-3 article'><h2>"+childData.title+"</h2><h3>"+childData.content+"</h3><div><p class='thumb_ups' id='thumb_ups"+(second_count-1).toString()+"'>"+childData.thumb_ups+"</p><a class='btn' href='#' onClick='return false;' id='thumb_btn"+(second_count-1).toString()+"'><i class='far fa-thumbs-up'></i><a><h6>"+childData.email+"</h6><h6>"+childData.time+"</h6><div id='comments"+(second_count-1).toString()+"'></div><textarea id='text"+(second_count-1).toString()+"' class='form-control' rows=2 placeholder='comment here'></textarea><button id='comment_btn"+(second_count-1).toString()+"' type='button' class='btn btn-primary mt-3 float-right'>Submit</button><br><br></div>\n";
                        document.getElementById('post_list').insertAdjacentHTML('afterbegin', total_post[total_post.length-1]);

                        var comment_btn = document.getElementById('comment_btn'+(second_count-1).toString());
                        var text = document.getElementById('text'+(second_count-1).toString());
                        comment_btn.addEventListener('click',function(){
                            var time = new Date();
                            var year = time.getFullYear().toString();
                            var month = time.getMonth();
                            var date = time.getDate().toString();
                            var hour = ("0"+time.getHours().toString()).substr(-2,2);
                            var minutes = ("0"+time.getMinutes().toString()).substr(-2,2);
                            var seconds = ("0"+time.getSeconds().toString()).substr(-2,2);
                            var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
                            var commentRef = firebase.database().ref("post_list/"+data.key+"/comments").push();
                            commentRef.set({
                                comment:text.value,
                                email: user.email,
                                time: months[month]+","+date+","+year+"   "+hour+":"+minutes+":"+seconds,
                            });
                            text.value = "";

                            var userRef = firebase.database().ref("users/"+(user.uid));
                            userRef.once('value').then(function (snapshot) {
                                var userData = snapshot.val();
                                var updates = {};
                                var reference = '/users/'+user.uid+'/comments';
                                console.log(userData);
                                updates[reference] = userData.comments+1; 
                                
                                firebase.database().ref().update(updates).catch(e => console.log(e.message));
                            })
                        });

                        var thumb_btn = document.getElementById('thumb_btn'+(second_count-1).toString());
                        var thumb_count = document.getElementById('thumb_ups'+(second_count-1).toString());
                        thumb_btn.addEventListener("click",function(){
                            var postRef = firebase.database().ref("post_list/"+(data.key));
                            postRef.once('value').then(function (snapshot) {
                                var postData = snapshot.val();
                                var updates = {};
                                var reference = '/post_list/'+data.key+'/thumb_ups';
                                updates[reference] = postData.thumb_ups+1; 
                                
                                firebase.database().ref().update(updates).catch(e => console.log(e.message));
                                thumb_count.innerHTML = postData.thumb_ups+1;

                                var userRef = firebase.database().ref("users/"+(postData.UID));
                                userRef.once('value').then(function (user1) {
                                    var userData = user1.val();
                                    var updates = {};
                                    var reference = '/users/'+postData.UID+'/thumb_ups';
                                    updates[reference] = userData.thumb_ups+1; 
                                    firebase.database().ref().update(updates);
                                })
                            })
                        });

                        firebase.database().ref('post_list/'+data.key+'/comments').on('child_added',function(comment){
                            var new_comment = document.createElement('div'); 
                            new_comment.innerHTML = "<div class='comment pl-3'><p>"+comment.val().comment+"</p><h6>"+comment.val().email+"</h6><h6>"+comment.val().time+"</h6></div>";
                            var putComment = document.getElementById('comments'+(second_count-1).toString());
                            //console.log('comments'+k.toString());
                            putComment.appendChild(new_comment);
                            
                        })
                    }
                });

                /* add listener for comments*/
                var j = 0;
                snapshot.forEach(function(post){
                    const k = j;
                    firebase.database().ref('post_list/'+post.key+'/comments').on('child_added',function(comment){
                        var new_comment = document.createElement('div'); 
                        new_comment.innerHTML = "<div class='comment pl-3'><p>"+comment.val().comment+"</p><h6>"+comment.val().email+"</h6><h6>"+comment.val().time+"</h6></div>";
                        var putComment = document.getElementById('comments'+k.toString());
                        console.log('comments'+k.toString());
                        putComment.appendChild(new_comment);
                    })
                    j++;
                })
                /* add listener for comments*/

                /* comment */ 
                var i = 0;
                snapshot.forEach(function (childSnapshot) {
                    var comment_btn = document.getElementById('comment_btn'+i.toString());
                    var text = document.getElementById('text'+i.toString());
                    var thumb_btn = document.getElementById('thumb_btn'+i.toString());
                    var thumb_count = document.getElementById('thumb_ups'+i.toString());
                    i++;
                    comment_btn.addEventListener('click',function(){
                        var time = new Date();
                        var year = time.getFullYear().toString();
                        var month = time.getMonth();
                        var date = time.getDate().toString();
                        var hour = ("0"+time.getHours().toString()).substr(-2,2);
                        var minutes = ("0"+time.getMinutes().toString()).substr(-2,2);
                        var seconds = ("0"+time.getSeconds().toString()).substr(-2,2);
                        var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
                        var commentRef = firebase.database().ref("post_list/"+childSnapshot.key+"/comments").push();
                        commentRef.set({
                            comment:text.value,
                            email: user.email,
                            time: months[month]+","+date+","+year+"   "+hour+":"+minutes+":"+seconds,
                        });
                        text.value = "";

                        var userRef = firebase.database().ref("users/"+(user.uid));
                        userRef.once('value').then(function (snapshot) {
                            var userData = snapshot.val();
                            var updates = {};
                            var reference = '/users/'+user.uid+'/comments';
                            updates[reference] = userData.comments+1; 
                            firebase.database().ref().update(updates);
                        })

                    });

                    thumb_btn.addEventListener("click",function(){
                        var postRef = firebase.database().ref("post_list/"+(childSnapshot.key));
                        postRef.once('value').then(function (snapshot) {
                            var postData = snapshot.val();
                            var updates = {};
                            var reference = '/post_list/'+childSnapshot.key+'/thumb_ups';
                            updates[reference] = postData.thumb_ups+1; 
                            
                            firebase.database().ref().update(updates).catch(e => console.log(e.message));

                            thumb_count.innerHTML = postData.thumb_ups+1;

                            var userRef = firebase.database().ref("users/"+(postData.UID));
                            userRef.once('value').then(function (user1) {
                                var userData = user1.val();
                                var updates = {};
                                var reference = '/users/'+postData.UID+'/thumb_ups';
                                updates[reference] = userData.thumb_ups+1; 
                                firebase.database().ref().update(updates);
                            })
                        })
                        
                    });

                })
                /* comment */
            })
            .catch(e => console.log(e.message));

        }
        else {
            Sign_in.innerHTML = "<a href='signin.html' id='signout'>Signin</a>";
            document.getElementById('post_list').innerHTML = "<br><h1>Sign in to view posts!</h1>";
        }
    });
}

window.onload = function(){
    init();
}