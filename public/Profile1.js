function init(){
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var Sign_in = document.getElementById('signin');
        var navBar = document.getElementById('nav');
        if (user) {
            user_email = user.email;
            Sign_in.innerHTML = "<a href='' id='signout'>Signout</a>";
            var Sign_out = document.getElementById('signout');
            var userInfo = document.getElementById('userInfo');
            userInfo.innerHTML += "<li><a href=''>"+user_email+"</a></li>";
            Sign_out.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });

            var userRef = firebase.database().ref("users/"+(user.uid));
            var profile = document.getElementById("profile");

            userRef.once('value').then(function (snapshot) {
                var data = snapshot.val();
                profile.innerHTML = "<h1>Profile</h1>"+"<h3>"+data.email+"</h3>"+"<h3>posts:"+data.posts+"</h3>"+"<h3>comments:"+data.comments+"</h3>"+"<h3>thumb ups received:"+data.thumb_ups+"</h3>";
            })

        } else {
            var profile = document.getElementById("profile");
            Sign_in.innerHTML = "<a href='signin.html' id='signout'>Signin</a>";
            profile.innerHTML = "<h1>You haven't signed in!</h1>";
        }
    });
}

window.onload = function(){
    init();
}