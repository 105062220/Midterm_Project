function init(){

    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var Sign_in = document.getElementById('signin');
        var navBar = document.getElementById('nav');
        if (user) {
            user_email = user.email;
            Sign_in.innerHTML = "<a href='' id='signout'>Signout</a>";
            var Sign_out = document.getElementById('signout');
            var userInfo = document.getElementById('userInfo');
            userInfo.innerHTML += "<li><a href=''>"+user_email+"</a></li>";
            Sign_out.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });

            post_btn = document.getElementById('post_btn');
            post_title = document.getElementById('title');
            post_content = document.getElementById('content');
            post_btn.addEventListener('click', function () {
                if (post_title.value != "" && post_content.value != "" && user_email != "") {          
                    var time = new Date();
                    var year = time.getFullYear().toString();
                    var month = time.getMonth();
                    var date = time.getDate().toString();
                    var hour = ("0"+time.getHours().toString()).substr(-2,2);
                    var minutes = ("0"+time.getMinutes().toString()).substr(-2,2);
                    var seconds = ("0"+time.getSeconds().toString()).substr(-2,2);
                    var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

                    var userRef = firebase.database().ref("users/"+(user.uid));
                    userRef.once('value').then(function (snapshot) {
                        var userData = snapshot.val();
                        var updates = {};
                        var reference = '/users/'+user.uid+'/posts';
                        updates[reference] = userData.posts+1; 
                        firebase.database().ref().update(updates);
                    })
                    

                    var newpostref = firebase.database().ref('post_list').push();
                    newpostref.set({
                        email: user_email,
                        title: post_title.value,
                        content: post_content.value,
                        time: months[month]+","+date+","+year+"   "+hour+":"+minutes+":"+seconds,
                        comments:"",
                        thumb_ups: 0,
                        UID: user.uid
                    }).then(function(){window.location = "index.html";}).catch(e => console.log(e.message));;
                    
                }
                else{
                    alert('Please Sign in first');
                }
            });

        }
        else {
            Sign_in.innerHTML = "<a href='signin.html' id='signout'>Signin</a>";
        }
    });

}

window.onload = function () {
    init();
}