function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var Sign_in = document.getElementById('signin');
        var navBar = document.getElementById('nav');
        if (user) {
            user_email = user.email;
            Sign_in.innerHTML = "<a href='' id='signout'>Signout</a>";
            var Sign_out = document.getElementById('signout');
            var userInfo = document.getElementById('userInfo');
            userInfo.innerHTML += "<li><a href=''>"+user_email+"</a></li>";
            Sign_out.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });

            var postsRef = firebase.database().ref("post_list");
            var max = -1;
            var hotest;
            postsRef.once('value').then(function (postList) {
                postList.forEach(function(post){
                    var postData = post.val();           
                    if(postData.thumb_ups > max){
                        max = postData.thumb_ups;
                        hotest = post.key;
                    }
                })
                var hot_post = document.getElementById("hotest");
                var hotPostData = postList.child(hotest).val();        
                hot_post.innerHTML = "<div class='my-5 p-3 article'><h2>"+hotPostData.title+"</h2><h3>"+hotPostData.content+"</h3><div><p class='thumb_ups' id='thumb_ups'>"+hotPostData.thumb_ups+"</p><a class='btn' href='#' onClick='return false;' id='thumb_btn'><i class='far fa-thumbs-up'></i></a><h6>"+hotPostData.email+"</h6></div><h6>"+hotPostData.time+"</h6></div>\n";
            });
         }
         else {
            Sign_in.innerHTML = "<a href='signin.html' id='signout'>Signin</a>";
            document.getElementById("hotestPost").innerHTML = "Sign in to view";
        }
    });
}

window.onload = function () {
    var randomNumber = Math.floor((Math.random() * 10) + 1);
    this.document.getElementById("body").style.backgroundImage = "url(images/bg"+randomNumber.toString()+".jpg)";
    init();
    if (Notification.permission !== "granted")
        Notification.requestPermission();

    var postsRef = firebase.database().ref("post_list");
    var first = true;
    postsRef.limitToLast(1).on('child_added',function(data){
        if(first == true) first = false;
        else{
            var notification = new Notification('New Post!', {
                icon: 'images/notification.png',
                body: "There is a new funny post, take a look!",
                });
        }
    })
}